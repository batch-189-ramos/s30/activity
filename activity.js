
db.fruits.insertMany([
        {
            "name": "Banana",
            "supplier": "Farmer Fruits Inc.",
            "stocks": 30,
            "price": 20,
            "onSale": true
        },
        {
            "name": "Mango",
            "supplier": "Mango Magic Inc.",
            "stocks": 50,
            "price": 70,
            "onSale": true
        },
        {
            "name": "Dragon Fruit",
            "supplier": "Farmer Fruits Inc.",
            "stocks": 10,
            "price": 60,
            "onSale": true
        },
        {
            "name": "Grapes",
            "supplier": "Fruity Co.",
            "stocks": 30,
            "price": 100,
            "onSale": true
        },
        {
            "name": "Apple",
            "supplier": "Apple Valley",
            "stocks": 0,
            "price": 20,
            "onSale": false
        },
        {
            "name": "Papaya",
            "supplier": "Fruity Co.",
            "stocks": 15,
            "price": 60,
            "onSale": true
        }
]);

// On sale Fruits
db.fruits.aggregate([
	{
		$match: {"onSale": true}
	},
	{
		$group: {_id: "fruits", fruitOnSale: {$sum:1}}
	},
	{
		$project: {_id: 0}
	}
]);


// Stock more than 20
db.fruits.aggregate([
	{
		$match: {"stocks": {
			$gt: 20
		}}
	},
	{
		$group: {_id: "fruits", total: {$sum:1}}
	},
	{
		$project: {_id: 0}
	}
]);

// Average Price

db.fruits.aggregate([
	{
		$match: {"onSale": true}
	},
	{
		$group: {_id:"$supplier", ave_price:{$avg: "$price"}}
	}
]);


// Max Price

db.fruits.aggregate([
	{
		$group: {_id: "$supplier", max_price: {$max: "$price"}}
	}
]);

// Min Price
db.fruits.aggregate([
	{
		$group: {_id: "$supplier", min_price: {$min: "$price"}}
	}
]);
